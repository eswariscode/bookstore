package com.bookstore.controller;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.bookstore.BookStoreApplication;
import com.bookstore.model.response.BaseResponse;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = BookStoreApplication.class, webEnvironment = WebEnvironment.DEFINED_PORT)
public class BookStoreAPITest {

	@Autowired
	private TestRestTemplate restTemplate;

	@Test
	public void testHealthCheck() throws Exception {
		String body = this.restTemplate.getForObject("/healthcheck", String.class);
		assertThat(body).contains("I am alive");
	}

	@Test
	public void testGetBooks() throws Exception {
		BaseResponse body = this.restTemplate.getForObject("/book", BaseResponse.class);
		assertThat(body.getResponseBody().getBooks().size()).isEqualTo(2);
	}

	@Test
	public void testGetUserInfoBeforeLogin() throws Exception {
		ResponseEntity<String> response = restTemplate.getForEntity("http://localhost:8080/user", String.class);
		System.out.println("Body " + response);
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
	}

}
