package com.bookstore;

import javax.servlet.Servlet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCacheManager;
import org.springframework.context.annotation.Bean;

import com.bookstore.controller.BookAPI;
import com.bookstore.controller.HealthCheckAPI;
import com.bookstore.controller.OrderAPI;
import com.bookstore.controller.UserAPI;
import com.bookstore.security.BookStoreAuth;
import com.bookstore.serializer.SkipNullGensonSerializer;
import com.dataheaps.aspectrest.AspectRestServlet;
import com.google.common.collect.ImmutableMap;

import lombok.extern.slf4j.Slf4j;

@SpringBootApplication
@EnableCaching
@Slf4j
public class BookStoreApplication {

	public static void main(String[] args) {
		SpringApplication.run(BookStoreApplication.class, args);
	}

	@Bean
	public ServletRegistrationBean<Servlet> servletRegistrationBean() {
		AspectRestServlet restServlet = new AspectRestServlet(true, new SkipNullGensonSerializer(),
				AspectRestServlet.getContext());
		restServlet.setModules(ImmutableMap.of("book", getBookAPI(), "user", getUserAPI(), "order", getOrderAPI(),
				"healthcheck", getHealthCheckAPI()));
		restServlet.setAuthenticators(ImmutableMap.of("auth", getBookStoreAuth()));
		return new ServletRegistrationBean<>(restServlet, "/");
	}

	@Bean
	public HealthCheckAPI getHealthCheckAPI() {
		return new HealthCheckAPI();
	}

	@Bean
	public BookAPI getBookAPI() {
		return new BookAPI();
	}

	@Bean
	public OrderAPI getOrderAPI() {
		return new OrderAPI();
	}

	@Bean
	public UserAPI getUserAPI() {
		return new UserAPI();
	}

	@Bean
	public BookStoreAuth getBookStoreAuth() {
		return new BookStoreAuth();
	}

}
