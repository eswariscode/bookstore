package com.bookstore.serializer;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import com.owlike.genson.Context;
import com.owlike.genson.Converter;
import com.owlike.genson.stream.ObjectReader;
import com.owlike.genson.stream.ObjectWriter;

/**
 * Custom Date Time Serializer
 *
 * @author eswari
 */
public class LocalDateTimeConvertor implements Converter<LocalDateTime> {

	private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd MMM yyyy HH:mm:ss");

	@Override
	public void serialize(LocalDateTime dateTime, ObjectWriter objectWriter, Context context) throws Exception {
		objectWriter.writeString(dateTime.format(dateTimeFormatter));
	}

	@Override
	public LocalDateTime deserialize(ObjectReader objectReader, Context context) throws Exception {
		return null;
	}
}
