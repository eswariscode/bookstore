package com.bookstore.serializer;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;

import org.joda.time.DateTime;

import com.dataheaps.aspectrest.serializers.JodaTimeConverter;
import com.dataheaps.aspectrest.serializers.Serializer;
import com.owlike.genson.Genson;
import com.owlike.genson.GensonBuilder;

public class SkipNullGensonSerializer implements Serializer {

    Genson genson = new GensonBuilder()
            .useClassMetadata(false)
            .setSkipNull(true)
            .useIndentation(true)
            .exclude("@class")
            .withConverter(new JodaTimeConverter(), DateTime.class)
            .withConverter(new LocalDateTimeConvertor(), LocalDateTime.class)
            .useDateAsTimestamp(true)
            .create();

	@Override
	public Object deserialize(InputStream i, Class<?> type) {
		return genson.deserialize(i, type);
	}

	@Override
	public byte[] serialize(Object i) {
		return genson.serialize(i).getBytes(StandardCharsets.UTF_8);
	}

	@Override
	public String getContentType() {
		return "application/json; charset=utf-8";
	}
}
