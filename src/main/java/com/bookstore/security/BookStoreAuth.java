package com.bookstore.security;

import com.bookstore.dao.TokenDAO;
import com.bookstore.dao.UserDAO;
import com.bookstore.entity.AuthToken;
import com.bookstore.entity.User;
import com.dataheaps.aspectrest.modules.auth.AbstractAuthModule;

import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;

@Slf4j
public class BookStoreAuth extends AbstractAuthModule<User> {

	@Autowired
	UserDAO userDAO;

	// TODO: token store can be improved by storing in cache
	@Autowired
	TokenDAO tokenDAO;

	@Override
	protected User authenticate(String identity, String password) throws IllegalAccessException, IOException {
		User user = userDAO.findByUserName(identity);
		if (user != null && StringUtils.equals(password, user.getPassword())) {
			return user;
		}
		throw new IllegalAccessException();
	}

	@Override
	protected String getUserToken(String identity, String password, User profile)
			throws IllegalAccessException, IOException {
		AuthToken authToken = tokenDAO.findByUserName(identity);
		if (authToken != null) {
			tokenDAO.delete(authToken);
		}
		String token = UUID.randomUUID().toString();
		tokenDAO.save(new AuthToken(identity, token));
		return token;
	}

	@Override
	protected User authenticateWithToken(String token) throws IllegalAccessException, IOException {
		AuthToken authToken = tokenDAO.findByToken(token);
		if (authToken != null) {
			User user = userDAO.findByUserName(authToken.getUserName());
			return user;
		}
		throw new IllegalAccessException();
	}
}
