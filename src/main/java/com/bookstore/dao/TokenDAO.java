package com.bookstore.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

import com.bookstore.entity.AuthToken;

@Component
public interface TokenDAO extends CrudRepository<AuthToken, Long>{
	AuthToken findByUserName(String userName);
	AuthToken findByToken(String token);
}
