package com.bookstore.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.bookstore.entity.OrderDetails;
import com.bookstore.entity.User;

public interface OrderDetailsDAO extends CrudRepository<OrderDetails, Long>{
	List<OrderDetails> findByUser(User user);
}
