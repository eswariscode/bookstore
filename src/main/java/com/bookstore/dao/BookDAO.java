package com.bookstore.dao;

import org.springframework.data.repository.CrudRepository;

import com.bookstore.entity.Book;

public interface BookDAO extends CrudRepository<Book, Long>{
	Book findByBookName(String bookName);
}
