package com.bookstore.dao;

import org.springframework.data.repository.CrudRepository;

import com.bookstore.entity.User;

public interface UserDAO extends CrudRepository<User, Long>{
	User findByUserName(String userName);
}
