package com.bookstore.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.owlike.genson.annotation.JsonIgnore;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@EqualsAndHashCode(of = { "id", "userName" })
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	String userName;
	private String password;
	String emailId;
	String mobileNumber;
	String addressLine1;
	String addressLine2;
	String city;
	String state;
	String zipCode;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "user", cascade = CascadeType.ALL)
	private List<OrderDetails> orders;

	@JsonIgnore
	public String getPassword() {
		return password;
	}

	@JsonIgnore
	public List<OrderDetails> getOrders() {
		return orders;
	}
	
	@JsonIgnore
	public Long getId() {
		return id;
	}
}
