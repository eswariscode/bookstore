package com.bookstore.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import com.bookstore.model.constants.Genre;
import com.owlike.genson.annotation.JsonIgnore;
import com.owlike.genson.annotation.JsonProperty;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@EqualsAndHashCode(of = { "id", "bookName" })
public class Book {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	String bookName;
	String author;
	@Column(name = "genre")
	private Integer genreId;
	@Transient
	String genreDisplayName;
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "book", cascade = CascadeType.ALL)
	private List<OrderDetails> orders;

	@JsonIgnore
	public Genre getGenreEnum() {
		return Genre.findById(this.genreId);
	}

	public void setGenreEnum(Genre genre) {
		this.genreId = genre.getId();
	}

	@JsonProperty("genre")
	public String getGenreDisplayName() {
		Genre genre = Genre.findById(this.genreId);
		return genre == null ? "" : genre.getDisplayName();
	}

	@JsonIgnore
	public Integer getGenreId() {
		return genreId;
	}

	@JsonIgnore
	public List<OrderDetails> getOrders() {
		return orders;
	}
	
	@JsonIgnore
	public Long getId() {
		return id;
	}
}
