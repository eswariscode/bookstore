package com.bookstore.controller;

import java.time.LocalDateTime;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;

import com.bookstore.dao.BookDAO;
import com.bookstore.dao.OrderDetailsDAO;
import com.bookstore.dao.UserDAO;
import com.bookstore.entity.Book;
import com.bookstore.entity.OrderDetails;
import com.bookstore.entity.User;
import com.bookstore.model.constants.Status;
import com.bookstore.model.request.BaseRequest;
import com.bookstore.model.request.RequestBody;
import com.bookstore.model.request.RequestHeader;
import com.bookstore.model.response.BaseResponse;
import com.bookstore.model.response.ResponseBody;
import com.bookstore.model.response.ResponseHeader;
import com.bookstore.serializer.SkipNullGensonSerializer;
import com.dataheaps.aspectrest.RestHandler;
import com.dataheaps.aspectrest.annotations.FromBody;
import com.dataheaps.aspectrest.annotations.Get;
import com.dataheaps.aspectrest.annotations.IsBody;
import com.dataheaps.aspectrest.annotations.Name;
import com.dataheaps.aspectrest.annotations.Path;
import com.dataheaps.aspectrest.annotations.Post;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.owlike.genson.Genson;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class BookAPI implements RestHandler {

	@Autowired
	private BookDAO bookDAO;

	@Override
	public void init() {
	}

	@Get
	@Path("")
	public BaseResponse getBooks() {
		BaseResponse baseResponse = null;
		try {
			List<Book> books = (List<Book>) bookDAO.findAll();
			if (books.isEmpty()) {
				baseResponse = BaseResponse.createBaseResposeHeaderOnly(Status.NORECORD_FOUND);
			} else {
				baseResponse = BaseResponse.createBaseRespose(Status.SUCCESS);
				baseResponse.getResponseBody().setBooks(books);
			}
		} catch (Exception e) {
			log.error("Exception occured while fetching books {}", e.getMessage());
			baseResponse = BaseResponse.createBaseResposeHeaderOnly(Status.TECHNICAL_ERROR);
		}
		return baseResponse;
	}

}
