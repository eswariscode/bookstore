package com.bookstore.controller;

import com.dataheaps.aspectrest.RestHandler;
import com.dataheaps.aspectrest.annotations.Get;
import com.dataheaps.aspectrest.annotations.Path;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class HealthCheckAPI  implements RestHandler {
	
	@Override
	public void init() {
	}
	
	@Get
	@Path("")
	public String ping() {
		return "I am alive";
	}
}
