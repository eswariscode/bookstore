package com.bookstore.controller;

import java.time.LocalDateTime;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.bookstore.dao.BookDAO;
import com.bookstore.dao.OrderDetailsDAO;
import com.bookstore.entity.Book;
import com.bookstore.entity.OrderDetails;
import com.bookstore.entity.User;
import com.bookstore.model.constants.Status;
import com.bookstore.model.request.BaseRequest;
import com.bookstore.model.response.BaseResponse;
import com.dataheaps.aspectrest.RestHandler;
import com.dataheaps.aspectrest.ServletContext;
import com.dataheaps.aspectrest.annotations.Authenticated;
import com.dataheaps.aspectrest.annotations.Get;
import com.dataheaps.aspectrest.annotations.IsBody;
import com.dataheaps.aspectrest.annotations.Name;
import com.dataheaps.aspectrest.annotations.Path;
import com.dataheaps.aspectrest.annotations.Post;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class OrderAPI implements RestHandler {

	@Autowired
	private BookDAO bookDAO;

	@Autowired
	private OrderDetailsDAO orderDetailsDAO;

	@Override
	public void init() {
	}

	@Get
	@Path("")
	@Authenticated
	public BaseResponse getOrders() {
		BaseResponse baseResponse = null;
		try {
			User user = ServletContext.getCurrentUser();
			log.info("Get Orders for user {}", user);
			List<OrderDetails> orders = (List<OrderDetails>) orderDetailsDAO.findByUser(user);
			if (orders.isEmpty()) {
				baseResponse = BaseResponse.createBaseResposeHeaderOnly(Status.NO_ORDERS_FOUND);
			} else {
				baseResponse = BaseResponse.createBaseRespose(Status.SUCCESS);
				baseResponse.getResponseBody().setOrders(orders);
			}
		} catch (Throwable e) {
			log.error("Exception occured while fetching orders {}", e.getMessage());
			baseResponse = BaseResponse.createBaseResposeHeaderOnly(Status.TECHNICAL_ERROR);
		}
		return baseResponse;
	}

	@Post
	@Path("")
	@Authenticated
	public BaseResponse orderBook(@IsBody @Name("id") BaseRequest baseRequest) {
		BaseResponse baseResponse = null;
		try {
			log.info("Order Book Request {}", baseRequest);
			Status validationStatus = isValidOrderRequest(baseRequest);
			if (validationStatus != Status.SUCCESS) {
				return BaseResponse.createBaseRespose(validationStatus);
			}

			User user = ServletContext.getCurrentUser();
			OrderDetails orderDetails = baseRequest.getRequestBody().getOrder();
			// check if book exists
			Book book = bookDAO.findByBookName(orderDetails.getBook().getBookName());
			if (book == null) {
				return BaseResponse.createBaseRespose(Status.NO_SUCH_BOOK);
			}
			
			baseResponse = saveOrder(baseRequest, user, orderDetails, book);

		} catch (Exception e) {
			log.error("Exception occured while ordering book {}", e.getMessage());
			baseResponse = BaseResponse.createBaseResposeHeaderOnly(Status.TECHNICAL_ERROR);
		}
		return baseResponse;
	}

	private BaseResponse saveOrder(BaseRequest baseRequest, User user, OrderDetails orderDetails, Book book) {
		BaseResponse baseResponse;
		orderDetails.setUser(user);
		orderDetails.setBook(book);
		setOrderDefaults(orderDetails);
		orderDetails = orderDetailsDAO.save(baseRequest.getRequestBody().getOrder());
		if (orderDetails == null) {
			baseResponse = BaseResponse.createBaseResposeHeaderOnly(Status.FAILED);
		} else {
			baseResponse = BaseResponse.createBaseRespose(Status.SUCCESS);
			baseResponse.getResponseBody().setOrder(orderDetails);
		}
		return baseResponse;
	}

	private void setOrderDefaults(OrderDetails orderDetails) {
		orderDetails.setOrderNo(RandomStringUtils.randomAlphanumeric(10).toUpperCase());
		orderDetails.setOrderDateTime(LocalDateTime.now());
		orderDetails.setOrderStatus("NEW");
	}

	private Status isValidOrderRequest(BaseRequest baseRequest) {
		OrderDetails orderDetails = baseRequest.getRequestBody().getOrder();
		if (orderDetails == null || orderDetails.getBook() == null
				|| StringUtils.isEmpty(orderDetails.getBook().getBookName())) {
			return Status.REQUIRED_PARAMS_MISSING;
		}
		return Status.SUCCESS;
	}

}
