package com.bookstore.controller;

import java.time.LocalDateTime;
import java.util.List;

import javax.servlet.http.Cookie;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;

import com.bookstore.dao.BookDAO;
import com.bookstore.dao.OrderDetailsDAO;
import com.bookstore.dao.UserDAO;
import com.bookstore.entity.Book;
import com.bookstore.entity.OrderDetails;
import com.bookstore.entity.User;
import com.bookstore.model.constants.Status;
import com.bookstore.model.request.BaseRequest;
import com.bookstore.model.request.RequestBody;
import com.bookstore.model.request.RequestHeader;
import com.bookstore.model.response.BaseResponse;
import com.bookstore.model.response.ResponseBody;
import com.bookstore.model.response.ResponseHeader;
import com.bookstore.serializer.SkipNullGensonSerializer;
import com.dataheaps.aspectrest.RestHandler;
import com.dataheaps.aspectrest.ServletContext;
import com.dataheaps.aspectrest.annotations.Authenticated;
import com.dataheaps.aspectrest.annotations.FromBody;
import com.dataheaps.aspectrest.annotations.Get;
import com.dataheaps.aspectrest.annotations.IsBody;
import com.dataheaps.aspectrest.annotations.Name;
import com.dataheaps.aspectrest.annotations.Path;
import com.dataheaps.aspectrest.annotations.Post;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.owlike.genson.Genson;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class UserAPI implements RestHandler {

	@Autowired
	private UserDAO userDAO;

	@Override
	public void init() {
	}

	@Get
	@Path("")
	@Authenticated
	public BaseResponse getUserInfo() {
		BaseResponse baseResponse = null;
		try {
			User user = ServletContext.getCurrentUser();
			User userDetail = userDAO.findByUserName(user.getUserName());
			if (userDetail == null) {
				baseResponse = BaseResponse.createBaseResposeHeaderOnly(Status.NO_SUCH_USER);
			} else {
				baseResponse = BaseResponse.createBaseRespose(Status.SUCCESS);
				baseResponse.getResponseBody().setUser(userDetail);
			}
		} catch (Exception e) {
			log.error("Exception occured while fetching users {}", e.getMessage());
			baseResponse = BaseResponse.createBaseResposeHeaderOnly(Status.TECHNICAL_ERROR);
		}
		return baseResponse;
	}

	@Post
	@Path("/register")
	public BaseResponse register(@IsBody @Name("id") BaseRequest baseRequest) {
		BaseResponse baseResponse = null;
		try {
			log.info("User registration request {}", baseRequest);
			if (isValidRegisterRequest(baseRequest)) {
				User user = userDAO.save(baseRequest.getRequestBody().getUser());
				if (user == null) {
					baseResponse = BaseResponse.createBaseResposeHeaderOnly(Status.FAILED);
				} else {
					baseResponse = BaseResponse.createBaseRespose(Status.SUCCESS);
					baseResponse.getResponseBody().setUser(user);
				}
			} else {
				baseResponse = BaseResponse.createBaseRespose(Status.REQUIRED_PARAMS_MISSING);
			}
		} catch (DataIntegrityViolationException e) {
			log.error("User already exists {}", baseRequest.getRequestBody().getUser().getUserName());
			baseResponse = BaseResponse.createBaseResposeHeaderOnly(Status.USER_ALREADY_EXISTS);
		} catch (Exception e) {
			log.error("Exception occured while registering user {}", e.getMessage());
			baseResponse = BaseResponse.createBaseResposeHeaderOnly(Status.TECHNICAL_ERROR);
		}
		return baseResponse;
	}

	private boolean isValidRegisterRequest(BaseRequest baseRequest) {
		if (baseRequest.getRequestBody().getUser() == null
				|| StringUtils.isEmpty(baseRequest.getRequestBody().getUser().getUserName())) {
			return false;
		}
		return true;
	}

}
