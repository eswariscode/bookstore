package com.bookstore.model.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ResponseHeader {
	String statusCode;
	String statusDescription;
	String requestIdentifier;

	public ResponseHeader(String statusCode, String statusDescription) {
		this.statusCode = statusCode;
		this.statusDescription = statusDescription;
	}
}
