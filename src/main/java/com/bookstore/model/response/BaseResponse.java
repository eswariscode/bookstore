package com.bookstore.model.response;

import com.bookstore.model.constants.Status;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class BaseResponse {
	ResponseHeader responseHeader;
	ResponseBody responseBody;

	public static BaseResponse createBaseRespose(Status status) {
		BaseResponse baseResponse = new BaseResponse();
		baseResponse.setResponseHeader(new ResponseHeader(status.getStatusCode(), status.getStatusDescription()));
		baseResponse.setResponseBody(new ResponseBody());
		return baseResponse;
	}

	public static BaseResponse createBaseResposeHeaderOnly(Status status) {
		BaseResponse baseResponse = new BaseResponse();
		baseResponse.setResponseHeader(new ResponseHeader(status.getStatusCode(), status.getStatusDescription()));
		return baseResponse;
	}
}
