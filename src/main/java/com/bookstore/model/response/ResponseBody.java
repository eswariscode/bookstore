package com.bookstore.model.response;

import java.util.List;

import com.bookstore.entity.Book;
import com.bookstore.entity.OrderDetails;
import com.bookstore.entity.User;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ResponseBody {
	List<User> users;
	List<Book> books;
	List<OrderDetails> orders;
	Book book;
	User user;
	OrderDetails order;
}