package com.bookstore.model.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RequestHeader {
	String requestIdentifier;
	String requestDateTime;
}
