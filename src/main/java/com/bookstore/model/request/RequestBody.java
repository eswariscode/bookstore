package com.bookstore.model.request;

import com.bookstore.entity.Book;
import com.bookstore.entity.OrderDetails;
import com.bookstore.entity.User;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RequestBody {

	User user;
	
	Book book;
	
	OrderDetails order;
}
