package com.bookstore.model.constants;

import java.util.HashMap;
import java.util.Map;

import lombok.Getter;

public enum Genre {

	COMIC(1, "Comic"), 
	CRIME_DETECTIVE(2, "Crime Detective"), 
	SCIENCE_FICTION(3, "Science Fiction"),
	SOFTWARE(4, "Software"), 
	BIG_DATA(5, "Big Data");

	@Getter
	int id;

	@Getter
	String displayName;

	private static Map<String, Genre> displayNameHolder = new HashMap<>();

	private static Map<Integer, Genre> idHolder = new HashMap<>();

	static {
		for (Genre genre : Genre.values()) {
			displayNameHolder.put(genre.getDisplayName(), genre);
			idHolder.put(genre.getId(), genre);
		}

	}

	Genre(int id, String displayName) {
		this.id = id;
		this.displayName = displayName;

	}

	public static Genre findByDisplayName(String displayName) {
		return displayNameHolder.get(displayName);
	}

	public static Genre findById(int id) {
		return idHolder.get(id);
	}

}
