package com.bookstore.model.constants;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum Status {

	SUCCESS("OK-200", "Success"), 
	FAILED("ERR-10001", "Failed"),
	TECHNICAL_ERROR("ERR-10002", "Technical error occured"),
	NORECORD_FOUND("ERR-10003", "No record found"),
	REQUIRED_PARAMS_MISSING("ERR-10004", "Required params missing"),
	USER_ALREADY_EXISTS("ERR-USR-1001", "User name taken already. Please provide some other user name"),
	NO_SUCH_USER("ERR-USR-1002", "Seems you are not registered with us yet"),
	NO_SUCH_BOOK("ERR-BK-1002", "No such book found in our store"),
	NO_ORDERS_FOUND("ERR-ORD-10003", "No orders found"),;
	
	@Getter
	String statusCode;
	
	@Getter
	String statusDescription;

}