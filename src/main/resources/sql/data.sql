insert into user (user_name, password) values('Sam', 'pwd123');
insert into user (user_name, password) values('Tony', 'pwd123');

insert into book (book_name, author, genre) values('Agile Practices', 'Barbee Davis', '4');
insert into book (book_name, author, genre) values('Big Data','Hrushikesha Mohanty', '5');

insert into order_details (order_no, book_id, user_id, order_date_time, quantity, order_status) values ('FGRR78999', 1, 1, CURRENT_TIMESTAMP, 1, 'NEW');
insert into order_details (order_no, book_id, user_id, order_date_time, quantity, order_status) values ('FGRR78994', 2, 1, CURRENT_TIMESTAMP, 1, 'NEW');
