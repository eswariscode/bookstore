create table user
(
   id integer auto_increment not null,
   user_name varchar(255) not null,
   password varchar(255) not null,
   email_id varchar(255),
   mobile_number varchar(10),
   address_line1 varchar(255),
   address_line2 varchar(255),
   city varchar(255),
   state varchar(255),
   zip_code varchar(255),
   primary key(id)
);

ALTER TABLE user
  ADD CONSTRAINT uq_user_name UNIQUE(user_name);

create table auth_token
(
   id integer auto_increment not null,
   user_name varchar(255) not null,
   token varchar(255) not null,
   primary key(id)
);

ALTER TABLE auth_token
  ADD CONSTRAINT uq_auth_user_name UNIQUE(user_name);
  
create table book
(
   id integer auto_increment not null,
   book_name varchar(255) not null,
   author varchar(255) not null,
   genre char(1) not null,
   primary key(id)
);

ALTER TABLE book
  ADD CONSTRAINT uq_book_name UNIQUE(book_name);

create table order_details
(
   id integer auto_increment not null,
   order_no varchar(10) not null,
   book_id integer not null,
   user_id integer not null,
   order_date_time timestamp not null,
   quantity integer not null,
   order_status char(10) not null,
   primary key(id)
);

alter table order_details add CONSTRAINT uq_order_no UNIQUE(order_no);
alter table order_details add foreign key (book_id) references book(id);
alter table order_details add foreign key (user_id) references user(id);
